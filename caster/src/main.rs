use std::{time, cmp::min, os::unix::io::AsRawFd, str, io::{self, ErrorKind, Write, Read}, process::Command, thread, env, cell::RefCell, thread::JoinHandle};
use signal_hook::{SIGWINCH, iterator::Signals};
use mio::{Events, Poll, Interest, Token, unix::SourceFd};
use ws::{connect, Message, Handshake, CloseCode, deflate::DeflateHandler};
use raw_tty::{TtyModeGuard};
use pty::fork::{Master, Fork};
use clap::{Arg, App, AppSettings};
use base64::encode;
use nix::{unistd::{Pid, read}, sys::signal::{Signal, kill}, errno::Errno};
use libc::{ioctl, TIOCGWINSZ, TIOCSWINSZ, FIONBIO};

use protocol::{JSONMessage, ChanMessage, Data, CtrlMessage, ChanInitData};

const STDIN:i32 = 0;

fn forward_winsize(src: i32, dst: i32) -> (u8, u8) {
    let mut winsize = libc::winsize {
        ws_row: 0,
        ws_col: 0,
        ws_xpixel: 0,
        ws_ypixel: 0,
    };
    unsafe {
        ioctl(src, TIOCGWINSZ, &mut winsize);
    };

    /* Limit terminal to a reasonnable size */
    winsize.ws_row = min(winsize.ws_row, 255);
    winsize.ws_col = min(winsize.ws_col, 255);

    unsafe {
        ioctl(dst, TIOCSWINSZ, &mut winsize);
    };
    (winsize.ws_col as u8, winsize.ws_row as u8)
}

fn set_nonblock(fd: i32) {
        unsafe { 
            let nonblocking = 1 as libc::c_int;
            ioctl(fd, FIONBIO, & nonblocking);
        }
}

fn send(stream: &mut ws::Sender, json_msg: &JSONMessage) -> ws::Result<()> {
    let text = serde_json::to_string(&json_msg).unwrap();
    stream.send(Message::text(&text))
}


fn stdin_to_pty(mut master: Master) {
    let _ = thread::spawn(move || {
        let mut buf = vec![0; 65536];
        let _ = master.write(b"PS1=\"[CAST ON] $PS1\" ; clear\n");
        loop {
            match io::stdin().read(&mut buf) {
                Ok(0) => panic!("Stdin was closed"),
                Ok(r) => {
                    let _ = master.write(&buf[0..r]);
                    ()
                },
                Err(e) => {
                    println!("Error reading stdin {:?}", e);
                    ()
                }
            }
        }
    });
}

fn pty_to_stdout(stream: &mut ws::Sender, master: Master, id: u32) -> ws::Result<()> {
    let mut buf = vec![0; 65536];
    let rawfd = master.as_raw_fd();
    set_nonblock(rawfd);
    let mut poll = Poll::new().unwrap();
    let mut master_source = SourceFd(&rawfd);
    poll.registry().register(&mut master_source, Token(0), Interest::READABLE).unwrap();
    let mut signals = Signals::new(&[SIGWINCH]).unwrap();
    poll.registry().register(&mut signals, Token(1), Interest::READABLE).unwrap();
    let mut events = Events::with_capacity(1024);

    loop {
        if let Err(e) = poll.poll(&mut events, None) {
            if e.kind() == ErrorKind::Interrupted { 
                continue; 
            }
            panic!("Error during poll: {:?}", e);
        }

        for signal in signals.pending() {
            if signal == SIGWINCH {
                let (new_cols, new_rows) = forward_winsize(STDIN, rawfd);
                let json_msg = JSONMessage::channel {
                    ts: None,
                    id: id,
                    data : ChanMessage::data {
                        data: Data::resize {
                            rows: new_rows,
                            cols: new_cols
                        },
                    },
                };
                send(stream, &json_msg)?;
                break;
            }
        }

        if !events.iter().any(|v| v.token() == Token(0)) {
            continue;
        }

        loop {
            /* Avoid flooding the server in case of heavy tty activity */
            thread::sleep(time::Duration::from_millis(50));
            match read(rawfd, &mut buf) {
                Ok(r) if r > 0 => {
                    io::stdout().write_all(&buf[0..r]).unwrap();
                    io::stdout().flush().unwrap();
                    match str::from_utf8(&buf[0..r]) {
                        Ok(msg) => {
                            let json_msg = JSONMessage::channel {
                                ts: None,
                                id: id,
                                data : ChanMessage::data {
                                    data: Data::action {
                                        content: msg.to_string(),
                                    },
                                },
                            };
                            send(stream, &json_msg)?;
                        },
                        _ => {}
                    };
                },
                Err(nix::Error::Sys(Errno::EAGAIN)) => {
                    break;
                },
                _ => {
                    println!("Process exited\r\n");
                    return Ok(())
                },
            }
        }
    }
}


fn handle_client(mut stream: ws::Sender, id: u32, width: u8, height: u8) {

    let fork = Fork::from_ptmx().expect("Unable to allocate pseudo-tty");
    if let Some(master) = fork.is_parent().ok() {
        let mut guard = TtyModeGuard::new(STDIN).expect("Unable to acquire stdin handle");
        guard.set_raw_mode().expect("Unable to set stdin to raw mode");

        stdin_to_pty(master.clone());
        match pty_to_stdout(&mut stream, master, id) {
            Err(_) => {
                println!("Server closed the connection, sending the child process the SIGHUP signal...\r");
                match fork {
                    Fork::Parent(pid, _) => { 
                        let _ = kill(Pid::from_raw(pid), Signal::SIGHUP);
                    },
                    _ => {},
                }
            },
            _ => {},
        }
       
        let _ = stream.close(CloseCode::Normal);
    } else {
        println!("TTY casting started");
        env::set_var("TERM", "xterm");
        env::set_var("LINES", format!("{}", height));
        env::set_var("COLUMNS", format!("{}", width));
        Command::new("/bin/sh").arg("-c").arg(format!("stty rows {}; stty columns {}; exec $SHELL", height, width)).status().unwrap();
    }
}

struct Handler<'a> { 
    stream: ws::Sender, 
    userpass: Option<(String, String)>, 
    thr: &'a RefCell<Option<JoinHandle<()> > >,
    width: u8,
    height: u8,
}

impl<'a> ws::Handler for Handler<'a> {
    fn on_open(&mut self, _ : Handshake) -> ws::Result<()> {
        println!("Connected to server");

        let json_msg = JSONMessage::hello {
            presenter: true,
        };
        let text = serde_json::to_string(&json_msg).unwrap();
        self.stream.send(Message::text(&text)).unwrap();
        println!("Server login....");

        let json_msg = JSONMessage::control {
            ts: None,
            ctl : CtrlMessage::create {
                scene: 1,
                id: None,
                init: ChanInitData::tty {
                    cols: self.width,
                    rows: self.height,
                    remote: "osef".to_string(),
                },
            },
        };
        let text = serde_json::to_string(&json_msg).unwrap();
        self.stream.send(Message::text(&text)).unwrap();
        println!("Channel create...");
        Ok(())
    }

    fn on_message(&mut self, msg: Message) -> ws::Result<()> {
        if msg.is_text() {
            let json_msg = serde_json::from_str(&msg.into_text().unwrap()) as serde_json::Result<JSONMessage>;
            match json_msg {
                Ok(JSONMessage::created {id, ..}) => {
                    let stream = self.stream.clone();
                    let (width, height) = (self.width, self.height);
                    let _thr = thread::spawn(move || {
                        println!("Started service thread");
                        handle_client(stream, id, width, height);
                    });
                    *self.thr.borrow_mut() = Some(_thr);
                },
                _ => {},
            }
        }
        Ok(())
    }

    fn build_request(&mut self, url: &url::Url) -> ws::Result<ws::Request> {
        println!("Building request for URL {}", url);
        let mut req = ws::Request::from_url(url);
        match &mut req {
            Ok(req) => { 
                match &self.userpass {
                    Some((username, password)) => {
                        let encoded_userpass = encode(format!("{}:{}", username, password));
                        let auth_header = format!("Basic {}", encoded_userpass);
                        req.headers_mut().push(("Authorization".to_string(), auth_header.into_bytes()));
                    },
                    _ => {}
                };
            },
            _ => {}
        };
        req
    }
}

fn main() {
    let matches = App::new("TTY caster")
        .version("0.1.0")
        .arg(
            Arg::with_name("username")
            .short("U")
            .long("username")
            .required(false)
            .takes_value(true)
            .help("username for HTTP basic auth")
        ).arg(
            Arg::with_name("server url")
            .short("u")
            .long("url")
            .required(true)
            .takes_value(true)
            .help("URL for server (format: ws://host:port/)")
        ).after_help("EXAMPLE\n    caster --url ws://127.0.0.1:8001\n")
        .setting(AppSettings::ArgRequiredElseHelp)
        .get_matches();

    let userpass = match matches.value_of("username") {
        Some(username) => {
            println!("Authenticating as: {}", username);
            Some((username.to_string(), rpassword::read_password_from_tty(Some("Password: ")).unwrap().to_string()))
        },
        _ => None,
    };

    if let Some((width, height)) = term_size::dimensions() {
        println!("Connecting to server"); 

        let jh = RefCell::new(None);
        let r_jh = &jh;
        connect(matches.value_of("server url").unwrap(), move |out| {
            DeflateHandler::new(Handler {stream:out, userpass: userpass.clone(), thr: r_jh, width: width as u8, height: height as u8})
        }).unwrap();

        match jh.into_inner() {
            Some(jh) => {
                jh.join().unwrap();
            }
            None => {},
        };
        println!("TTY casting stopped...");
    } else {
        println!("Can't determine terminal size");
    }
}
