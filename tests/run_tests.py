#!/usr/bin/env python3
import socket
import os
import sys
import traceback
import termios
import atexit
import signal
import asyncio
import websockets
import json

async def send(role, ws, s):
	await ws.send(s)
	print("[" + role + "] send: ", s)

async def recv(role, ws):
	r = await ws.recv()
	print("[" + role + "] recv: ", r)
	return r
	
async def expect(role, ws, w):
	r = await recv(role, ws)
	j = json.loads(r)
	w["ts"] = j["ts"]
	if j != w:
		print("[" + role + "] ERROR: Expected: ", w)
		print("[" + role + "] ERROR: Received: ", json.loads(r))
		raise ValueError("Test failed")
	print("[" + role + "] Received expected message.")

async def viewer():
	await asyncio.sleep(0.5)
	async with websockets.connect(uri) as ws:
		try:
			txt = json.dumps({"type":"hello", "presenter": False})
			await send("VIEW", ws, txt)
			print("[VIEW] connected!")
			#global metadata
			await expect("VIEW", ws, {"type":"globalmetadata", "name":"author", "value": "Tester1"})
			await expect("VIEW", ws, {"type":"focus", "id": 0})
			#chan 0 info
			await expect("VIEW", ws, {"type":"control", "op":"create", "id": 0, "scene": 1, "chanType": "pdf", "init": {"file": "http://tartempion.com/test2.pdf", "initial_page": 2}})
			await expect("VIEW", ws, {"type":"channel", "id": 0, "subtype":"metadata", "name": "title", "value":"My little pdf"})

			#end initial state

			#chan 2
			await expect("VIEW", ws, {"type":"control","id":2,"op":"create","scene":1,"chanType":"tty","init":{"cols":100,"rows":32,"remote":"127.0.0.1:4001"}})

			#chan 3
			await expect("VIEW", ws, {"type":"control", "op":"create", "id": 3, "scene": 1, "chanType": "pdf", "init": {"file": "http://tartempion.com/test.pdf", "initial_page": 1}})

			#chan 4
			await expect("VIEW", ws, {"type":"control", "op":"create", "id": 4, "scene": 1, "chanType": "chat", "init": {}})

			#change global metadata
			await expect("VIEW", ws, {"type":"globalmetadata", "name":"author", "value": "Tester2"})
			print("[VIEW] Entering loop")
			while True:
				data = await recv("VIEW", ws)
		except websockets.exceptions.ConnectionClosed:
			print("[VIEW] disconnected")
		except:
			traceback.print_exc()
			os.kill(os.getpid(), signal.SIGTERM)

async def presenter():
	async with websockets.connect(uri) as ws:
		try:
			pass
			txt = json.dumps({"type":"hello", "presenter": True})
			await send("PREZ", ws, txt)
			print("[PREZ] connected!")

			#set some metadata (global)
			await send("PREZ", ws, json.dumps({"type":"globalmetadata", "name":"author", "value": "Tester1"}))
			await expect("PREZ", ws, {"type":"globalmetadata", "name":"author", "value": "Tester1"})

			#create a pdf channel
			await send("PREZ", ws, json.dumps({"type":"control", "op":"create", "id": 0, "scene": 1, "chanType": "pdf", "init": {"file": "http://tartempion.com/test2.pdf", "initial_page": 1}}))
			await expect("PREZ", ws, {"type":"control", "op":"create", "id": 0, "scene": 1, "chanType": "pdf", "init": {"file": "http://tartempion.com/test2.pdf", "initial_page": 1}})

			#change page for pdf 
			await send("PREZ", ws, json.dumps({"type":"channel", "id": 0, "subtype": "data", "data": { "type": "page", "page": 2}}))
			await expect("PREZ", ws, {"type":"channel", "id": 0, "subtype": "data", "data": { "type": "page", "page": 2}})

			#focus it
			await send("PREZ", ws, json.dumps({"type":"focus", "id": 0}))
			await expect("PREZ", ws, {"type":"focus", "id": 0})


			#set some metadata for it
			await send("PREZ", ws, json.dumps({"type":"channel", "id": 0, "subtype":"metadata", "name": "title", "value":"My little pdf"}))
			await expect("PREZ", ws, {"type":"channel", "id": 0, "subtype":"metadata", "name": "title", "value":"My little pdf"})

			await asyncio.sleep(1)

			#create tty channel
			await send("PREZ", ws, json.dumps({"type":"control", "op":"create", "id": 2, "scene": 1, "chanType": "tty", "init": {"rows": 32, "cols": 100, "remote": "127.0.0.1:4444"} }))
			await expect("PREZ", ws, {"type":"control","id":2,"op":"create","scene":1,"chanType":"tty","init":{"cols":100,"rows":32,"remote":"127.0.0.1:4444"}})

			#create a pdf channel
			await send("PREZ", ws, json.dumps({"type":"control", "op":"create", "id": 3, "scene": 1, "chanType": "pdf", "init": {"file": "http://tartempion.com/test.pdf", "initial_page": 1}}))
			await expect("PREZ", ws, {"type":"control", "op":"create", "id": 3, "scene": 1, "chanType": "pdf", "init": {"file": "http://tartempion.com/test.pdf", "initial_page": 1}})

			#create a cha channel
			await send("PREZ", ws, json.dumps({"type":"control", "op":"create", "id": 4, "scene": 1, "chanType": "chat", "init": {}}))
			await expect("PREZ", ws, {"type":"control", "op":"create", "id": 4, "scene": 1, "chanType": "chat", "init": {}})

			#set some metadata (global)
			await send("PREZ", ws, json.dumps({"type":"globalmetadata", "name":"author", "value": "Tester2"}))
			await expect("PREZ", ws, {"type":"globalmetadata", "name":"author", "value": "Tester2"})
			print("[PREZ] Entering loop")
			while True:
				data = await recv("PREZ", ws)
		except websockets.exceptions.ConnectionClosed:
			print("[PREZ] disconnected")
		except:
			traceback.print_exc()
			os.kill(os.getpid(), signal.SIGTERM)

uri = sys.argv[1]
print("Connecting...")
task_presenter = asyncio.get_event_loop().create_task(presenter())
asyncio.get_event_loop().run_until_complete(viewer())

    
