mod sockserv;
mod updateman;
mod shared;
mod muxer;
mod demuxer;

use std::{io::Write, sync::mpsc::channel, process};
use clap::{Arg, App, AppSettings};
use log::{info, LevelFilter};
use env_logger::Builder;

use sockserv::{EventListener, SockServ};

fn server(bind_url:&str, presenter_url:&str, tick_delay: u32) {
    let (event_sender, event_receiver) = channel();
    let event_listener = EventListener::new(event_receiver, event_sender.clone(), tick_delay);

    if bind_url != presenter_url {
        info!("Binding on {} for presenter", presenter_url);
        let sock_serv = SockServ::new(event_sender.clone(), presenter_url.to_string(), false);
        sock_serv.launch();
    }
    let sock_serv = SockServ::new(event_sender.clone(), bind_url.to_string(), bind_url != presenter_url);
    info!("Binding on {} for clients", bind_url);
    sock_serv.launch();


    event_listener.run();
}

fn main()  {
    let matches = App::new("Websocket server").version("0.1.0")
        .arg(
            Arg::with_name("tick delay")
            .short("t")
            .long("tick-delay")
            .required(false)
            .takes_value(true)
            .help("Tick delay (milliseconds, defaults to 50, don't modify unless you know what you're doing)")
        )
        .arg(
            Arg::with_name("log level")
            .short("l")
            .long("log-level")
            .required(false)
            .takes_value(true)
            .possible_values(&["debug", "info", "warn"])
            .help("Set logging level")
        )
        .arg(
            Arg::with_name("bind address")
            .short("a")
            .long("addr")
            .required(true)
            .takes_value(true)
            .help("host:port for clients")
        )
        .arg(
            Arg::with_name("presenter bind address")
            .short("p")
            .long("presenter")
            .required(true)
            .takes_value(true)
            .help("host:port for presenters (uses <bind url> if omitted)")
        ).after_help("EXAMPLE\n    server --presenter 127.0.0.1:8001 --addr 127.0.0.1:8000\n")
        .setting(AppSettings::ArgRequiredElseHelp)
        .get_matches();

    let bind_url = matches.value_of("bind address").unwrap();
    let presenter_url = matches.value_of("presenter bind address").unwrap_or(bind_url);

    let filter = match matches.value_of("log level") {
        Some(level) if level == "warn" => LevelFilter::Warn,
        Some(level) if level == "info" => LevelFilter::Info,
        Some(level) if level == "debug" => LevelFilter::Debug,
        _ => LevelFilter::Error, 
    };

    let tick_delay = match matches.value_of("tick delay") {
        Some(val) => {
            match val.parse() {
                Ok(val) => { val },
                _ => {
                    println!("error: The tick delay should be an integer (try --help)");
                    process::exit(1);
                }
            }
        },
        _ => { 50 }
    };

    Builder::new()
    .format(|buf, record| {
        writeln!(buf, "[{}] - {}", record.level(), record.args())
    })
    .filter(None, filter)
    .init();

    server(&bind_url, &presenter_url, tick_delay);
}


