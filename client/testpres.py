#!/usr/bin/env python3
import socket
import os
import sys
import termios
import atexit
import asyncio
import websockets
import json

async def client():
    uri = sys.argv[1]
    async with websockets.connect(uri) as ws:
        try:
            print("Connected!")
            txt = json.dumps({"type":"hello", "presenter": True})
            await ws.send(txt)
            print("receiving...")
            txt = await ws.recv()
            print("received")

            txt = json.dumps({"type":"control", "op":"create", "id": 3, "scene": 1, "chanType": "tty", "init": {"rows": 32, "cols": 100, "remote": "127.0.0.1:4444"}, })
            print("send: ", txt)
            await ws.send(txt)
            txt = json.dumps({"type":"control", "op":"create", "id": 4, "scene": 1, "chanType": "pdf", "init": {"file": "https://klemm.7un.net/files/conneriedujour.pdf", "initial_page": 4}})
            print("send: ", txt)
            await ws.send(txt)
            
            while True:
                await asyncio.sleep(5.0)
                txt = json.dumps({"type" : "focus", "id": 3})
                await ws.send(txt)
                await asyncio.sleep(5.0)
                txt = json.dumps({"type" : "focus", "id": 4})
                await ws.send(txt)
            while True:
                print("receiving")
                data = await ws.recv()
                print("recv: ", data)
        except websockets.exceptions.ConnectionClosed:
            print("Connection closed")


print("Connecting...")
asyncio.get_event_loop().run_until_complete(client())
