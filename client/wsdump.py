#!/usr/bin/env python3
import socket
import os
import sys
import termios
import atexit
import asyncio
import websockets
import json

async def client():
    uri = sys.argv[1]
    async with websockets.connect(uri) as ws:
        try:
            print("Connected!")
            await ws.send(json.dumps({"type":"hello", "presenter": True}))
            hdr_received = False
            seek = None
            while True:
                data = await ws.recv()
                if isinstance(data, bytes):
                    print("bin data received\n");
                    if hdr_received:
                        ts = 0
                        p = 1
                        for i in range(21, 13, -1):
                            ts = ts + data[i]*p
                            p = p * 256
                        print("cluster received, ts=" , ts)
                        #patch
                        if seek is not None:
                            data = bytearray(data)
                            ts = ts - seek
                            for i in range(21, 13, -1):
                                data[i] = ts % 256
                                ts = ts // 256
                        pass
                    sys.stderr.buffer.write(data)
                    hdr_received = True
                else:
                    print("text data: ", data)
                    j = json.loads(data)
        except websockets.exceptions.ConnectionClosed:
            print("Connection closed")


print("Connecting...")
asyncio.get_event_loop().run_until_complete(client())

    
