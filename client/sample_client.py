#!/usr/bin/env python3
from aioconsole import ainput
import getpass
import socket
import os
import sys
import asyncio
import websockets
import json
import base64

loop = asyncio.get_event_loop()

ws = None
chans = {}
async def send(ws, msg):
    if ws is None:
        print("Not connected.")
    else:
        await ws.send(json.dumps(msg))

async def recv(ws):
    return json.loads(await ws.recv())

async def handle_websocket(url):
    global ws
    global chans
    headers = None
    if user is not None:
        print("Authenticating as " + user)
        password=getpass.getpass(prompt='Password: ', stream=None)
        auth = base64.b64encode((user + ":" + password).encode("utf-8")).decode("utf-8")
        headers = { "Authorization" : "Basic " + auth }
    try:
        ws = await websockets.connect(url, extra_headers=headers)
        await send(ws, {"type":"hello", "presenter": True})

        while True:
            msg = await recv(ws)
            if msg["type"] == "control" and msg["op"] == "create":
                chanid = msg["id"]
                label = "tty"
                if msg["chanType"] == "pdf":
                    label = msg["init"]["file"]
                chans[str(chanid)] = label
            if msg["type"] == "control" and msg["op"] == "delete":
                chanid = msg["id"]
                label = "tty"
                try:
                    del chans[str(chanid)]
                except:
                    pass

    except OSError as e:
        print("Disconnected: ", e)
    except asyncio.CancelledError:
        print("Cancelled")
        await ws.close()

async def handle_commandline():
    try:
        while True:
            print("> ", end="", flush=True)
            txt = await ainput()
            tab = txt.split()
            if len(tab) == 0:
                continue
            cmd = tab[0]
            if cmd == "q":
                break
            elif cmd == "o" and len(tab) > 1:
                pdf = tab[1]
                await send(ws, {"type":"control", "op":"create", "scene": 1, "chanType": "pdf", "init": {"file": pdf, "initial_page": 1}})

            elif cmd == "f" and len(tab) > 1:
                if tab[1] in chans:
                    await send(ws, {"type":"focus", "id": int(tab[1])})
                else:
                    print("Invalid channel ID")
            elif cmd == "p" and len(tab) > 2:
                if tab[1] in chans:
                    try:
                        page = int(tab[2])
                        await send(ws, {"type":"channel", "id": int(tab[1]), "subtype": "data", "data": { "type": "page", "page": page }})
                    except ValueError:
                        print("Invalid page number")
                else:
                    print("Invalid channel ID")
            elif cmd == "k" and len(tab) > 1:
                if tab[1] in chans:
                    await send(ws, {"type":"control", "op": "delete", "id" : int(tab[1])})
                else:
                    print("Invalid channel ID")
            elif cmd == "c":
                for k in chans:
                    print("Channel " + str(k) + ": " + chans[k])
            else:
                print("Available commands:")
                print("  o <url> (open PDF)")
                print("  c (list channels)")
                print("  f <chan id> (focus channel)")
                print("  p <chan id> <page num> (change page on PDF)")
                print("  k <chan id> (kill channel)")
                print("  q (quit)")


    except asyncio.CancelledError:
        print("Quitting")

if len(sys.argv) < 2:
    print("Usage: ./sample_client.py ws://host:port/ [<username for http basic auth>]")
else:
    user = None
    if len(sys.argv) > 2:
        user = sys.argv[2]
    task_web = loop.create_task(handle_websocket(sys.argv[1]))
    task_cmd = loop.create_task(handle_commandline())
    done, pending = loop.run_until_complete(asyncio.wait({task_web, task_cmd}, return_when=asyncio.FIRST_COMPLETED))
    for t in pending:
        t.cancel()
        loop.run_until_complete(t)
